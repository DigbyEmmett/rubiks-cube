import java.lang.Math;
import java.util.*;
import java.io.*;
class rubiks{
    char[] FACES = {'B','R','W','O','G','Y'};
    static int[] indexes = {0,2,3,5};
    enum row_col {
        ROW,
        COL;
    }
    char[] values;
    public static void main(String[] args) {
        rubiks R = new rubiks();
        try {
            while(true) {
                System.out.printf("%d",System.in.available());
                String line = System.console().readLine();
                
                System.out.printf("%s",line);
            }
        }
        catch(IOException e) {
            System.out.println("Error");
        }
    }
    public rubiks() {
        values = new char[54];
        for(int i=0;i<6;i++) {
            for(int j=0;j<9;j++) {
                values[i*9 + j] = FACES[i];
            }
        }
    }
    void Print() {
        for(int j=0;j<3;j++) {
            System.out.print("    ");
            for(int k=0;k<3;k++) {
                System.out.print(values[4*9+j*3+k]);
            }
            System.out.println();
        }
        for(int i=0;i<3;i++) {
            for(int j=0;j<4;j++) {
                for(int k=0;k<3;k++) {
                    System.out.print(values[indexes[j]*9+i*3+k]);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        for(int j=0;j<3;j++) {
            System.out.print("    ");
            for(int k=0;k<3;k++) {
                System.out.print(values[9+j*3+k]);
            }
            System.out.println();
        }
    }
    int index(int row, int col) {
        return 3*row+col;
    }
    void R(boolean dir,row_col rc,int ind) {
        int sf = 2;//current face starts as the front face
        boolean rowcol = rc==row_col.ROW;
        char ct = '\0',st = '\0';//current tile, swap tile
        int d = b(dir);
        int nu=0;
        for(int j=0;j<3;j++) {
            sf = 2;
            if(rc==row_col.ROW && dir == true)
                nu = 1;
            else if(rc==row_col.ROW && dir == false)
                nu = -2;
            else if(rc==row_col.COL && dir == true)
                nu = 2;
            else
                nu = -1;
            for(int i=0;i<5;i++) {
                //there are 3 tiles to move for each face and 5 face swaps
                if(rowcol) {
                    ct = values[sf*9+index(ind,j)];
                    values[sf*9+index(ind,j)] = st;
                }
                else {
                    ct = values[sf*9+index(j,ind)];
                    values[sf*9+index(j,ind)] = st;
                }
                st = ct;

                sf = Math.floorMod(sf + nu,6);
                nu = nu + b(nu==-2) - b(nu==-1) + b(nu==1) - b(nu==2);
            }
        }
    }
    int b(boolean a){
        return a?1:0;
    }
}
