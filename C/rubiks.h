typedef enum{
    AC=1,
    C=-1
} DIR;
typedef enum {
    ROW=1,
    COL=0
} ORD;
typedef struct {
    char values[54];/* holds 54 values but it is
            mallocd to unbind from function calls*/
} Cube;
Cube* createCube();
int cIndex(int,int);
void printCube(Cube*);
void rotate(Cube*,int,ORD,DIR);
void scramble(Cube*,int);
void rotateFace(Cube*,int,DIR);
