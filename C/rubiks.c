#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "rubiks.h"
#define PI 3.14159
char FACES[] = {'B','R','W','O','G','Y'};
int indexes[] = {0,2,3,5};/* for indexing the side faces when printing */
Cube* createCube() {
    Cube* cube;
    int i,j,k;
    cube = (Cube*)malloc(sizeof(Cube)); 
    char* values = cube->values;
    for(i=0;i<6;i++) {
        for(j=0;j<9;j++) {
            values[i*9+j] = FACES[i];
        }
    }
    return cube;
}
int cIndex(int row,int col) {
    return 3*row+col;
}
void printCube(Cube* cube) {
    //top first
    int i,j,k;
    char* values = cube->values;
    for(j=0;j<3;j++) {
        printf("    ");
        for(k=0;k<3;k++) {
            printf("%c",values[4*9+j*3+k]);
        }
        printf("\n");
    }
    for(i=0;i<3;i++) {
        for(j=0;j<4;j++) {
            for(k=0;k<3;k++) {
                printf("%c",values[indexes[j]*9+i*3+k]);
            }
            printf(" ");
        }
        printf("\n");
    }
    for(int j=0;j<3;j++) {
        printf("    ");
        for(k=0;k<3;k++) {
            printf("%c",values[9+j*3+k]);
        }
        printf("\n");
    }
}
void rotate(Cube* cube,int pos,ORD ord,DIR dir) { //pos is 0 to 3 (exclusive),dir is direction

    // initially swap the base stuff so it makes sense with the rotation
    // which is (c+9)%36 and we are swapping everything in the string

        int sf = 2;//current face starts as the front face
        int row = ord==ROW;
        int tmp_nu;
        int face = -1;
        char ct = '\0',st = '\0';//current tile, swap tile
        int nu;
        int ind=0;
        char* values = cube->values;
        for(int j=0;j<3;j++) {
            sf = 2;
            if(row && dir)
                nu = 1;
            else if(row && dir==-1)
                nu = -2;
            else if(!row && dir)
                nu = 2;
            else
                nu = -1;
            for(int i=0;i<5;i++) {
                //there are 3 tiles to move for each face and 5 face swaps
                if(row) {
                   ind = cIndex(pos,j); 
                }
                else {
                    ind = cIndex(j,pos);
                }
                ct = values[sf*9+ind];
                values[sf*9+ind] = st;
                st = ct;

                sf = (sf + nu) % 6;
                nu = nu + (nu==-2) - (nu==-1) + (nu==1) - (nu==2);
            }
        }
        //first find the face which is turned.
        if(row) {
            if(pos==0) {
                face = 1;
            }
            else if(pos==3) {
                face = 4;
            }
        }
        else {
            if(pos==0) {
                face = 0;
            }
            else if(pos==3) {
                face = 3; 
            }
        }
        if(face==-1){return;}
        rotateFace(cube,face,dir);
    
}
void rotateFace(Cube* cube,int face, DIR dir) {
    int x = -1,y = -1,tmpx;
    char st,ct;
    int i,j;
    static int transforms[2][2] = {{-1,-1},{-1,0}};
    int cx = 0;
    int cy = dir;
    char* values = cube->values;
    printf("Rotating %d in %d\n",face,dir);
    for(j=0;j<2;j++) {
        x = transforms[j][0];
        y = transforms[j][1];
        for(i=0;i<5;i++) {
            ct = values[face*9+(x+1)*3+(y+1)];
            values[face*9+(x+1)*3+(y+1)] = st;
            st = ct;
            tmpx = -y*cy;
            y = x*cy;
            x = tmpx;
            printf("transform; x: %d y: %d\n",x,y);
        }
    }
}
void scramble(Cube* cube, int count) {
    int i;
    int rIndex, rOrd, rDir;
    for(i=0;i<count;i++) {
        rIndex = rand()%3;
        rOrd = rand()%2;
        rDir = rand()%2;
        rDir = -(rDir==0) + rDir;
        rotate(cube,rIndex,rOrd,rDir);
        printCube(cube);
    }
}
