#include <assert.h>
#include <stdio.h>
#include "rubiks.h"
int main(){
    Cube* a;
    int i;
    char colours[] = {'l','u','f','r','d','b'};
    char b;
    char face[9];
    a = createCube();

    printCube(a);

    scramble(a,2);

    printCube(a);

    return (0);
}
